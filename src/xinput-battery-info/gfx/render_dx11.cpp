#include "render_dx11.h"
#include <SDL2/SDL_syswm.h>
#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_dx11.h"
#include "../imgui/imgui_impl_sdl.h"
#include "../ui/images.h"
#include "../xinput_info.h"

#define CLEANUP_D3D_RESOURCE(res) if (res) { (res)->Release(); (res) = nullptr; }

RenderDX11::RenderDX11() :
    ui_(std::make_unique<UserInterface>())
{
    ui_->SetContinueRun(&continueRun_);
}

void RenderDX11::Run()
{
    if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0)
        return;

    const auto windowFlags = static_cast<SDL_WindowFlags>(SDL_WINDOW_RESIZABLE | SDL_WINDOW_ALLOW_HIGHDPI);
    window_ = SDL_CreateWindow(
        "XInput Battery Info",
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        270, 90,
        windowFlags);

    SDL_SysWMinfo wmInfo;
    SDL_VERSION(&wmInfo.version);
    SDL_GetWindowWMInfo(window_, &wmInfo);

    auto* const hWnd = wmInfo.info.win.window;
    if (!CreateD3DDevice(hWnd))
    {
        CleanupD3DDevice();
        return;
    }

    // Load images embedded in the binary.
    imgLoader_ = std::make_unique<ImageLoaderDX11>(d3dDevice_);

    ui_->AddImage(images::BatteryIcon::Unknown,
        imgLoader_->LoadTexture(images::battery_warning_96_img, images::battery_warning_96_len));

    ui_->AddImage(images::BatteryIcon::Empty,
        imgLoader_->LoadTexture(images::battery_empty_96_img, images::battery_empty_96_len));

    ui_->AddImage(images::BatteryIcon::Low,
        imgLoader_->LoadTexture(images::battery_low_96_img, images::battery_low_96_len));

    ui_->AddImage(images::BatteryIcon::Medium,
        imgLoader_->LoadTexture(images::battery_medium_96_img, images::battery_medium_96_len));

    ui_->AddImage(images::BatteryIcon::Full,
        imgLoader_->LoadTexture(images::battery_full_96_img, images::battery_full_96_len));

    // Setup the ImGui context.
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO(); (void)io;
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
    io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
    //io.ConfigViewportsNoAutoMerge = true;
    //io.ConfigViewportsNoTaskBarIcon = true;
    io.IniFilename = nullptr;

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();

    // When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
    ImGuiStyle& style = ImGui::GetStyle();
    if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
    {
        style.WindowRounding = 0.0f;
        style.Colors[ImGuiCol_WindowBg].w = 1.0f;
    }

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForD3D(window_);
    ImGui_ImplDX11_Init(d3dDevice_, d3dDeviceContext_);

    auto clear_color = ImVec4(0.1f, 0.1f, 0.1f, 1.00f);

    int winHeight = 0, winWidth = 0;

    // Main loop
    while (continueRun_)
    {
        // Poll and handle events (inputs, window resize, etc.)
        // You can read the io.WantCaptureMouse, io.WantCaptureKeyboard flags to tell if dear imgui wants to use your inputs.
        // - When io.WantCaptureMouse is true, do not dispatch mouse input data to your main application.
        // - When io.WantCaptureKeyboard is true, do not dispatch keyboard input data to your main application.
        // Generally you may always pass all inputs to dear imgui, and hide them from your application based on those two flags.
        SDL_Event event;
        while (SDL_PollEvent(&event))
        {
            ImGui_ImplSDL2_ProcessEvent(&event);
            if (event.type == SDL_QUIT)
                continueRun_ = false;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_CLOSE && event.window.windowID == SDL_GetWindowID(window_))
                continueRun_ = false;
            if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_RESIZED && event.window.windowID == SDL_GetWindowID(window_))
            {
                // Release all outstanding references to the swap chain's buffers before resizing.
                CleanupRenderTarget();
                pSwapChain_->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0);
                CreateRenderTarget();
            }
        }

        // Get the size of the window so we can draw a window in ImGui.
        SDL_GetWindowSize(window_, &winWidth, &winHeight);

        // Start the Dear ImGui frame
        ImGui_ImplDX11_NewFrame();
        ImGui_ImplSDL2_NewFrame(window_);
        ImGui::NewFrame();

        XInputInfoCollector::UpdateBatteryInfo();

        // Draw the user interface
        ui_->Draw();

        // Rendering
        ImGui::Render();
        d3dDeviceContext_->OMSetRenderTargets(1, &mainRenderTargetView_, nullptr);
        d3dDeviceContext_->ClearRenderTargetView(mainRenderTargetView_, reinterpret_cast<float*>(&clear_color));
        ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());

        // Update and Render additional Platform Windows
        if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
        }

        pSwapChain_->Present(1, 0); // Present with vsync
        //g_pSwapChain->Present(0, 0); // Present without vsync
    }

    // Cleanup
    ImGui_ImplDX11_Shutdown();
    ImGui_ImplSDL2_Shutdown();
    ImGui::DestroyContext();

    CleanupD3DDevice();
    SDL_DestroyWindow(window_);
    SDL_Quit();
}

bool RenderDX11::CreateD3DDevice(HWND hWnd)
{
    // Setup swap chain
    DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory(&sd, sizeof(sd));

    sd.BufferCount                        = 2;
    sd.BufferDesc.Width                   = 0;
    sd.BufferDesc.Height                  = 0;
    sd.BufferDesc.Format                  = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator   = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.Flags                              = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
    sd.BufferUsage                        = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow                       = hWnd;
    sd.SampleDesc.Count                   = 1;
    sd.SampleDesc.Quality                 = 0;
    sd.Windowed                           = TRUE;
    sd.SwapEffect                         = DXGI_SWAP_EFFECT_DISCARD;

    const UINT createDeviceFlags = 0;
    //createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
    D3D_FEATURE_LEVEL featureLevel;
    const D3D_FEATURE_LEVEL featureLevelArray[2] = { D3D_FEATURE_LEVEL_11_0, D3D_FEATURE_LEVEL_10_0, };

    const auto result = D3D11CreateDeviceAndSwapChain(nullptr,
        D3D_DRIVER_TYPE_HARDWARE,
        nullptr,
        createDeviceFlags,
        featureLevelArray,
        2,
        D3D11_SDK_VERSION,
        &sd,
        &pSwapChain_,
        &d3dDevice_,
        &featureLevel,
        &d3dDeviceContext_);

    if (result != S_OK)
        return false;

    CreateRenderTarget();
    return true;
}

void RenderDX11::CleanupD3DDevice()
{
    CleanupRenderTarget();
    CLEANUP_D3D_RESOURCE(pSwapChain_);
    CLEANUP_D3D_RESOURCE(d3dDeviceContext_);
    CLEANUP_D3D_RESOURCE(d3dDevice_);
}

void RenderDX11::CreateRenderTarget()
{
    ID3D11Texture2D* pBackBuffer;
    pSwapChain_->GetBuffer(0, IID_PPV_ARGS(&pBackBuffer));

    if (pBackBuffer == nullptr)
        return;

    d3dDevice_->CreateRenderTargetView(pBackBuffer, nullptr, &mainRenderTargetView_);
    pBackBuffer->Release();
}

void RenderDX11::CleanupRenderTarget()
{
    CLEANUP_D3D_RESOURCE(mainRenderTargetView_);
}
