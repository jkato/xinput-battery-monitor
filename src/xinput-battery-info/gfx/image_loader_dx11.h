#pragma once
#include <unordered_map>
#include "../platform.h"
#include <d3d11.h>
#include "image_data.h"

class ImageLoaderDX11
{
    ID3D11Device* d3dDevice_;

public:
    ImageLoaderDX11(ID3D11Device* device);

    image_data LoadTexture(uint8_t* image, size_t bufferSize) const;

private:
    bool LoadImageDX11(uint8_t* buffer, size_t bufferSize, void** texture, int* width, int* height) const;
};