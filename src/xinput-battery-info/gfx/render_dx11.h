#pragma once
#include <memory>
#include "../platform.h"
#include <d3d11.h>
#include <SDL2/SDL.h>
#include "../ui/ui.h"
#include "image_loader_dx11.h"

class RenderDX11
{
    bool continueRun_ = true;

    ID3D11Device*           d3dDevice_            = nullptr;
    ID3D11DeviceContext*    d3dDeviceContext_     = nullptr;
    IDXGISwapChain*         pSwapChain_           = nullptr;
    ID3D11RenderTargetView* mainRenderTargetView_ = nullptr;

    SDL_Window* window_ = nullptr;

    std::unique_ptr<ImageLoaderDX11> imgLoader_ = nullptr;
    std::unique_ptr<UserInterface> ui_;

public:
    RenderDX11();

    void Run();

private:
    bool CreateD3DDevice(HWND hWnd);
    void CleanupD3DDevice();

    void CreateRenderTarget();
    void CleanupRenderTarget();
};
