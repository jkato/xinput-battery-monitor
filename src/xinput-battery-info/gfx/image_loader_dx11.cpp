#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>
#include "image_loader_dx11.h"

ImageLoaderDX11::ImageLoaderDX11(ID3D11Device* device) :
    d3dDevice_(device)
{
}

image_data ImageLoaderDX11::LoadTexture(uint8_t* image, size_t bufferSize) const
{
    auto img = image_data{};

    const auto loadSuccess = LoadImageDX11(image, bufferSize, &img.texture, &img.width, &img.height);
    if (!loadSuccess)
        return {};

    return img;
}

bool ImageLoaderDX11::LoadImageDX11(uint8_t* buffer, size_t bufferSize, void** texture, int* width, int* height) const
{
    auto** d3d11Texture = reinterpret_cast<ID3D11ShaderResourceView**>(texture);

    auto imgWidth = 0;
    auto imgHeight = 0;
    auto* imageData = stbi_load_from_memory(buffer, static_cast<int32_t>(bufferSize),
        &imgWidth, &imgHeight,
        nullptr, STBI_ORDER_RGB);

    if (imageData == nullptr)
        return false;

    D3D11_TEXTURE2D_DESC desc;
    ZeroMemory(&desc, sizeof desc);
    desc.Width            = imgWidth;
    desc.Height           = imgHeight;
    desc.MipLevels        = 1;
    desc.ArraySize        = 1;
    desc.Format           = DXGI_FORMAT_R8G8B8A8_UNORM;
    desc.SampleDesc.Count = 1;
    desc.Usage            = D3D11_USAGE_DEFAULT;
    desc.BindFlags        = D3D11_BIND_SHADER_RESOURCE;
    desc.CPUAccessFlags   = 0;

    ID3D11Texture2D* pTexture = nullptr;
    D3D11_SUBRESOURCE_DATA subResource;
    subResource.pSysMem          = imageData;
    subResource.SysMemPitch      = desc.Width * 4;
    subResource.SysMemSlicePitch = 0;
    d3dDevice_->CreateTexture2D(&desc, &subResource, &pTexture);

    if (pTexture == nullptr)
        return false;

    D3D11_SHADER_RESOURCE_VIEW_DESC view;
    ZeroMemory(&view, sizeof view);
    view.Format              = DXGI_FORMAT_R8G8B8A8_UNORM;
    view.ViewDimension       = D3D11_SRV_DIMENSION_TEXTURE2D;
    view.Texture2D.MipLevels = desc.MipLevels;
    d3dDevice_->CreateShaderResourceView(pTexture, &view, d3d11Texture);

    *width  = imgWidth;
    *height = imgHeight;
    stbi_image_free(imageData);

    return true;
}
