#pragma once

struct image_data
{
    int height;
    int width;
    void* texture;
};