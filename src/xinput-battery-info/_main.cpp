#define SDL_MAIN_HANDLED
#include <atomic>
#include <chrono>
#include <memory>
#include <thread>
#include <stb_image.h>
#include "gfx/render_dx11.h"
#include "platform.h"
#include "xinput_info.h"

static RenderDX11* renderer = nullptr;

static std::atomic_bool poll_xinput{};

void poll_battery_info();
void run();

INT WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR lpCommandLine, INT nCmdShow)
{
    const auto render = std::make_unique<RenderDX11>();
    renderer = render.get();

    poll_xinput.exchange(true);

    std::thread renderThread(run);
    renderThread.join();
    poll_xinput.exchange(false);

    renderer = nullptr;

    return 0;
}

void poll_battery_info()
{
    if (CoInitialize(nullptr) != S_OK)
        return;

    const auto delay = std::chrono::milliseconds(250);

    while(poll_xinput)
    {
        XInputInfoCollector::UpdateBatteryInfo();
        std::this_thread::sleep_for(delay);
    }

    CoUninitialize();
}

void run() { renderer->Run(); }