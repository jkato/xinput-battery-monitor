#pragma once
#include <cstdint>

namespace images
{
    enum class BatteryIcon
    {
        Unknown,
        Empty,
        Low,
        Medium,
        Full,
    };

    extern uint8_t battery_empty_96_img[325];
    constexpr size_t battery_empty_96_len = sizeof(battery_empty_96_img) / sizeof(uint8_t);

    extern uint8_t battery_full_96_img[317];
    constexpr size_t battery_full_96_len = sizeof(battery_full_96_img) / sizeof(uint8_t);

    extern uint8_t battery_low_96_img[331];
    constexpr size_t battery_low_96_len = sizeof(battery_low_96_img) / sizeof(uint8_t);

    extern uint8_t battery_medium_96_img[331];
    constexpr size_t battery_medium_96_len = sizeof(battery_medium_96_img) / sizeof(uint8_t);

    extern uint8_t battery_warning_96_img[346];
    constexpr size_t battery_warning_96_len = sizeof(battery_warning_96_img) / sizeof(uint8_t);
}