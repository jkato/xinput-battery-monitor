#pragma once
#include <unordered_map>
#include "images.h"
#include "../gfx/image_data.h"
#include "../xinput_info.h"

class UserInterface
{
    std::unordered_map<images::BatteryIcon, image_data> images_;

    bool* continueRun_ = nullptr;

public:
    void AddImage(images::BatteryIcon id, image_data img);

    void Draw() const;
    void SetContinueRun(bool* continueRun);

private:
    void DrawAboutDialog() const;

    void DrawControllerInfo(battery_info* info) const;

    void DrawMainContent() const;
    void DrawMainMenu() const;
};