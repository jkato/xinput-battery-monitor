#include "../imgui/imgui.h"
#include "../platform.h"
#include "ui.h"

static const ImGuiWindowFlags default_window_flags =
    ImGuiWindowFlags_NoMove |
    ImGuiWindowFlags_NoCollapse |
    ImGuiWindowFlags_NoTitleBar |
    ImGuiWindowFlags_NoResize |
    ImGuiWindowFlags_NoBringToFrontOnFocus |
    ImGuiWindowFlags_MenuBar |
    ImGuiWindowFlags_NoDocking;

const char IMGUI_LICENSE[] = R"(The MIT License (MIT)

Copyright (c) 2014-2020 Omar Cornut

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.)";

const char SDL2_LICENSE[] = R"(This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.)";

void UserInterface::AddImage(images::BatteryIcon id, image_data img)
{
    images_.try_emplace(id, img);
}

void UserInterface::Draw() const
{
    const auto* viewport = ImGui::GetMainViewport();

    ImGui::SetNextWindowPos(viewport->Pos);
    ImGui::SetNextWindowSize(viewport->Size);
    ImGui::Begin("_", nullptr, default_window_flags);
    DrawMainMenu();
    DrawMainContent();
    ImGui::End();

    DrawAboutDialog();
}

void UserInterface::SetContinueRun(bool* continueRun) { continueRun_ = continueRun; }

static bool show_about_dialog = false;
void UserInterface::DrawAboutDialog() const
{
    if (!show_about_dialog) return;

    ImGui::SetNextWindowSize(ImVec2{ 555.0f, 300.0f }, ImGuiCond_FirstUseEver);

    ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 2.5f);
    ImGui::Begin("About", &show_about_dialog, ImGuiWindowFlags_NoCollapse);
    ImGui::PopStyleVar();

    ImGui::Text("Battery icons made available by Icons8");
    ImGui::Text("https://icons8.com");
    if (ImGui::IsItemHovered())
    {
        ImGui::SetMouseCursor(ImGuiMouseCursor_Hand);
        ImGui::BeginTooltip();
        ImGui::Text("Click to open in browser.");
        ImGui::EndTooltip();
    }
    if (ImGui::IsItemClicked())
    {
        ShellExecute(nullptr, nullptr, L"https://icons8.com", nullptr, nullptr, SW_SHOW);
    }
    ImGui::Text("\n");

    ImGui::Separator();
    ImGui::Text("imgui");
    ImGui::Separator();
    ImGui::Text(IMGUI_LICENSE);

    ImGui::Separator();
    ImGui::Text("SDL2");
    ImGui::Separator();
    ImGui::Text(SDL2_LICENSE);

    ImGui::End();
}

void UserInterface::DrawControllerInfo(battery_info* info) const
{
    static const ImVec2 icon_size = { 48.0f, 48.0f };

    if (info->device_id < 0) return;

    const auto* viewport = ImGui::GetMainViewport();

    const char* levelName;
    image_data icon;
    switch (info->battery_level)
    {
    case BATTERY_LEVEL_EMPTY:
        icon = images_.at(images::BatteryIcon::Empty);
        levelName = "Empty";
        break;
    case BATTERY_LEVEL_LOW:
        icon = images_.at(images::BatteryIcon::Low);
        levelName = "Low";
        break;
    case BATTERY_LEVEL_MEDIUM:
        icon = images_.at(images::BatteryIcon::Medium);
        levelName = "Medium";
        break;
    case BATTERY_LEVEL_FULL:
        icon = images_.at(images::BatteryIcon::Full);
        levelName = "Full";
        break;
    default:
        icon = images_.at(images::BatteryIcon::Unknown);
        levelName = "Unknown";
        break;
    }

    ImGui::Text("Controller %d\nLevel: %s", info->device_id + 1, levelName);
    ImGui::SameLine();

    ImGui::SetCursorPosX((viewport->Size.x - icon_size.x) - 10.0f);
    ImGui::Image(icon.texture, icon_size);
    if (ImGui::IsItemHovered())
    {
        const char* typeName;
        switch (info->battery_type)
        {
        case BATTERY_TYPE_ALKALINE:
            typeName = "Alkaline";
            break;
        case BATTERY_TYPE_DISCONNECTED:
            typeName = "Disconnected";
            break;
        case BATTERY_TYPE_NIMH:
            typeName = "NiMH";
            break;
        case BATTERY_TYPE_WIRED:
            typeName = "Wired";
            break;
        default:
            typeName = "Unknown";
            break;
        }

        ImGui::BeginTooltip();
        ImGui::Text("Type: %s", typeName);
        ImGui::EndTooltip();
    }

    ImGui::Separator(); 
}

void UserInterface::DrawMainContent() const
{
    auto* deviceInfo = XInputInfoCollector::GetBatteryInfo();

    for (auto idx = 0; idx < XUSER_MAX_COUNT; idx++)
        DrawControllerInfo(&deviceInfo[idx]);
}

void UserInterface::DrawMainMenu() const
{
    if (ImGui::BeginMainMenuBar())
    {
        if (ImGui::BeginMenu("App"))
        {
            if (ImGui::MenuItem("Exit", "Alt+F4")) { *continueRun_ = !*continueRun_; }
            ImGui::EndMenu();
        }

        if (ImGui::BeginMenu("Help"))
        {
            if (ImGui::MenuItem("About")) { show_about_dialog = !show_about_dialog; }
            ImGui::EndMenu();
        }

        ImGui::EndMainMenuBar();
    }
}
