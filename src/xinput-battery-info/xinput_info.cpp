#include "xinput_info.h"

battery_info XInputInfoCollector::devices_[XUSER_MAX_COUNT];

battery_info* XInputInfoCollector::GetBatteryInfo()
{
    return devices_;
}

void XInputInfoCollector::UpdateBatteryInfo()
{
    for (auto idx = 0; idx < XUSER_MAX_COUNT; idx++)
        UpdateDeviceBatteryInfo(idx);
}

void XInputInfoCollector::UpdateDeviceBatteryInfo(int32_t idx)
{
    static XINPUT_STATE state = { };
    static XINPUT_BATTERY_INFORMATION info = { };

    ZeroMemory(&state, sizeof(XINPUT_STATE));
    ZeroMemory(&info, sizeof(XINPUT_BATTERY_INFORMATION));

    const auto id = static_cast<DWORD>(idx);

    auto result = XInputGetState(id, &state);
    if (result == ERROR_SUCCESS)
        devices_[idx].device_id = idx;
    else
    {
        devices_[idx].device_id = -1;
        return;
    }

    result = XInputGetBatteryInformation(id, XINPUT_DEVTYPE_GAMEPAD, &info);
    if (result != ERROR_SUCCESS)
    {
        devices_[idx].device_id = -1;
        return;
    }

    devices_[idx].battery_level = info.BatteryLevel;
    devices_[idx].battery_type = info.BatteryType;
}
