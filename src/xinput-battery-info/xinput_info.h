#pragma once
#include <cstdint>
#include <memory>
#include "platform.h"
#include <Xinput.h>

/**
*\brief XInput Battery Information.
*/
struct battery_info
{
    uint8_t battery_level;
    uint8_t battery_type;
    int32_t device_id;
};

class XInputInfoCollector
{
    static battery_info devices_[XUSER_MAX_COUNT];

public:
    static battery_info* GetBatteryInfo();
    static void UpdateBatteryInfo();

private:
    static void UpdateDeviceBatteryInfo(int32_t idx);
};
