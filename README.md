# XInput Battery Monitor

Displays battery information about XInput devices.

![screenshot](./doc/img/screenshot1.png)